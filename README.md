# Exemplo de uso da API Marvel #

Este dojo visa interagir com a api da marvel. A aplicação busca informação dos personagens enviados e, relaciona quais quadrinhos eles podem serem encontrados na mesma revista.

### Como fazer o setup da aplicação? ###

Para executar a aplicação, apenas digite:

mvn spring-boot:run

Para submeter as requisições use o Swagger Io. Acesse a url http://localhost:8080/swagger-ui.html, com o swagger io para ser executados os endpoints da api.

Dúvidas, por favor, entre em contato comigo:

Marcos Leandro Francischinelli <leandro.francischinelli@gmail.com>