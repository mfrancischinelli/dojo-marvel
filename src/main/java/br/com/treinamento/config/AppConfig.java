package br.com.treinamento.config;

import br.com.treinamento.dojo.model.Comic;
import br.com.treinamento.dojo.model.MarvelCharacter;
import br.com.treinamento.dojo.model.Relation;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.common.collect.TreeBasedTable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Configuration
@EnableAutoConfiguration
@ComponentScan("br.com.treinamento.dojo")
@EnableSwagger2
public class AppConfig {
	public static void main(String[] args) {
		SpringApplication.run(AppConfig.class, args);
	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
			.select()
			.apis(RequestHandlerSelectors.any())
			.paths(PathSelectors.any())
			.build();
	}

	@Bean
	@PostConstruct
	public Map<String, Relation> relationsBean() {
		return new ConcurrentHashMap<>();
	}

	@Bean
	@PostConstruct
	public Map<String, Comic> comicsBean() {
		return new ConcurrentHashMap<>();
	}

	@Bean
	@PostConstruct
	public Map<String, MarvelCharacter> charactersBean() {
		return new ConcurrentHashMap<>();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
			.title("Hands on CI&T challenge")
			.description("Spring REST Sample with Swagger")
			.contact(new Contact("Marcos Leandro Francischinelli",
				"https://www.linkedin.com/in/marcos-leandro-francischinelli-24562020?trk=nav_responsive_tab_profile",
				"leandro.francischinelli@gmail.com"))
			.license("Apache License Version 2.0")
			.licenseUrl("https://github.com/IBM-Bluemix/news-aggregator/blob/master/LICENSE")
			.version("0.0.1-SNAPSHOT")
			.build();
	}
}
