package br.com.treinamento.dojo.exception;

/**
 * Created by leandro on 10/11/16.
 */
public class MarvelCharacterNotFoundException extends RuntimeException {
  public MarvelCharacterNotFoundException(String msg){
    super(msg);
  }
}
