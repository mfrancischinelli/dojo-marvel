package br.com.treinamento.dojo.exception;

/**
 * Created by leandro on 20/11/16.
 */
public class MarvelApiRequestError extends RuntimeException {
  public MarvelApiRequestError(String msg) {
    super(msg);
  }
}
