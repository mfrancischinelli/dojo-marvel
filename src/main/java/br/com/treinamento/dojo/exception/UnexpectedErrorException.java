package br.com.treinamento.dojo.exception;

/**
 * Created by leandro on 20/11/16.
 */
public class UnexpectedErrorException extends RuntimeException {
  public UnexpectedErrorException(String msg){
    super(msg);
  }
}
