package br.com.treinamento.dojo.exception;

/**
 * Created by leandro on 19/11/16.
 */
public class RelationNotFoundException extends RuntimeException {
  public RelationNotFoundException(String msg){
    super(msg);
  }
}
