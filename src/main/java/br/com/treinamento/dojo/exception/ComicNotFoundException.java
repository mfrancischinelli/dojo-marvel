package br.com.treinamento.dojo.exception;

/**
 * Created by leandro on 19/11/16.
 */
public class ComicNotFoundException extends RuntimeException {
  public ComicNotFoundException(String msg){
    super(msg);
  }
}
