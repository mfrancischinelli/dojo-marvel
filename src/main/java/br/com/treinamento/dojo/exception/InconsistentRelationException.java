package br.com.treinamento.dojo.exception;

/**
 * Created by leandro on 19/11/16.
 */
public class InconsistentRelationException extends RuntimeException {
  public InconsistentRelationException(String msg) {
    super(msg);
  }
}
