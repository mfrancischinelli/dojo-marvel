package br.com.treinamento.dojo.exception;

import org.springframework.validation.Errors;

/**
 * Created by leandro on 20/11/16.
 */
public class InvalidParameterException extends RestControllerException {
  public InvalidParameterException(String msg, Errors errors){
    super(msg, errors);
  }
}
