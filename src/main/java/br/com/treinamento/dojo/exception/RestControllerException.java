package br.com.treinamento.dojo.exception;

import org.springframework.validation.Errors;

/**
 * Created by leandro on 20/11/16.
 */
public class RestControllerException extends RuntimeException {
  private Errors errors;

  public RestControllerException(String message, Errors errors) {
    super(message);
    this.errors = errors;
  }

  public Errors getErrors() { return errors; }
}
