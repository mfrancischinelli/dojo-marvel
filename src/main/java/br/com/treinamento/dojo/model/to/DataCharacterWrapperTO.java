package br.com.treinamento.dojo.model.to;

import br.com.treinamento.dojo.model.MarvelCharacter;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Tolerate;

import java.util.List;

/**
 * Created by leandro on 20/11/16.
 */
@Data
@Builder
@EqualsAndHashCode
public class DataCharacterWrapperTO {
  private List<MarvelCharacter> results;

  @Tolerate
  public DataCharacterWrapperTO() {
  }

}

