package br.com.treinamento.dojo.model.to;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Tolerate;

/**
 * Created by leandro on 20/11/16.
 */
@Data
@Builder
@EqualsAndHashCode
public class MarvelHashTO {
  private String hashTimestamp;
  private String hashValue;
  private String apiKey;

  @Tolerate
  public MarvelHashTO() {
  }
}
