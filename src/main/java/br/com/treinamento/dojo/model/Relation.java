package br.com.treinamento.dojo.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by leandro on 10/11/16.
 */
@Data
@Builder
@EqualsAndHashCode
public class Relation {
  private String characterId;
  private String relatedCharacterId;
  private List<String> comics;

  @Tolerate
  public Relation() {}
}
