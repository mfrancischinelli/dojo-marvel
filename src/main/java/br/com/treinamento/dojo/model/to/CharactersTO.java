package br.com.treinamento.dojo.model.to;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Tolerate;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by leandro on 19/11/16.
 */
@Data
@Builder
@EqualsAndHashCode
public class CharactersTO {
  private String characterName;
  private String relatedCharacterName;

  @Tolerate
  public CharactersTO() {
  }
}
