package br.com.treinamento.dojo.model;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Tolerate;

import java.util.List;

/**
 * Created by leandro on 10/11/16.
 */
@Data
@Builder
@EqualsAndHashCode
public class Comic {
  @SerializedName("id")
  private String comicId;

  @SerializedName("title")
  private String comicName;

  private String description;

  @Tolerate
  public Comic() {}
}
