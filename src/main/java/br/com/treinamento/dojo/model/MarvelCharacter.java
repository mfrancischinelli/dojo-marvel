package br.com.treinamento.dojo.model;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Tolerate;

/**
 * Created by leandro on 10/11/16.
 */
@Data
@Builder
@EqualsAndHashCode
public class MarvelCharacter {
  @SerializedName("id")
  private String characterId;

  @SerializedName("name")
  private String characterName;

  private String description;

  @Tolerate
  public MarvelCharacter() {}
}
