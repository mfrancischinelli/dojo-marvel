package br.com.treinamento.dojo.model.to;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Tolerate;

/**
 * Created by leandro on 19/11/16.
 */
@Data
@Builder
@EqualsAndHashCode
public class MarvelWrapperTO<T> {
  private T data;

  @Tolerate
  public MarvelWrapperTO() {
  }
}
