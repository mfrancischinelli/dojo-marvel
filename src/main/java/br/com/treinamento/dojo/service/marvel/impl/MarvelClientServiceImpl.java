package br.com.treinamento.dojo.service.marvel.impl;

import br.com.treinamento.dojo.exception.MarvelApiRequestError;
import br.com.treinamento.dojo.model.Comic;
import br.com.treinamento.dojo.model.MarvelCharacter;
import br.com.treinamento.dojo.model.Relation;
import br.com.treinamento.dojo.model.to.*;
import br.com.treinamento.dojo.service.marvel.MarvelAuthenticationService;
import br.com.treinamento.dojo.service.marvel.MarvelClientService;
import br.com.treinamento.dojo.service.relation.CharacterService;
import br.com.treinamento.dojo.service.relation.ComicService;
import br.com.treinamento.dojo.service.relation.RelationService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by leandro on 09/11/16.
 */
@Service
public class MarvelClientServiceImpl implements MarvelClientService {
  @Autowired
  private CharacterService characterService;

  @Autowired
  private ComicService comicService;

  @Autowired
  private RelationService relationService;

  @Autowired
  private MarvelAuthenticationService authenticationService;

  @Value("${marvel.api.url.retrieve.character}")
  private String urlCharacter;

  @Override
  public RelationTO save(CharactersTO characters) {
    RelationTO relationTO = null;
    final MarvelCharacter character = instanceCharacter(characters.getCharacterName());
    final MarvelCharacter relatedCharacter = instanceCharacter(characters.getRelatedCharacterName());

    Optional<Map.Entry<String, Relation>> relationFound =
      relationService.findRelationByCharacterAndRelated(character.getCharacterId(), relatedCharacter.getCharacterId());

    if (relationFound.isPresent()) {
      relationTO = relationService.instanceRelationTO(relationFound.get().getKey(), relationFound.get().getValue());
    } else {

      List<Comic> characterComics = retrieveCharacterComicsFromMarvel(character.getCharacterId());
      List<Comic> relatedCharacterComics = retrieveCharacterComicsFromMarvel(relatedCharacter.getCharacterId());

      List<Comic> relatedComics = retrieveAllRelatedComics(characterComics, relatedCharacterComics);

      if (!relatedComics.isEmpty()) {
        Relation relation =
          Relation.builder()
            .characterId(character.getCharacterId())
            .relatedCharacterId(relatedCharacter.getCharacterId())
            .comics(relatedComics.stream().map(Comic::getComicId).collect(Collectors.toList())).build();
        relationTO = relationService.save(relation);
      }
    }
    return relationTO;
  }

  private MarvelCharacter instanceCharacter(String characterName) {
    MarvelCharacter relatedCharacter;
    Optional<MarvelCharacter> optionalCharacter = characterService.findCharacterByName(characterName);
    if(optionalCharacter.isPresent()) {
      relatedCharacter = optionalCharacter.get();
    } else {
      relatedCharacter = retrieveCharacterFromMarvel(characterName);
    }
    return relatedCharacter;
  }

  private List<Comic> retrieveAllRelatedComics(List<Comic> characterComics, List<Comic> relatedCharacterComics) {
    List<Comic> relatedComics = new ArrayList<>();
    characterComics.stream().forEach(charItem ->
      relatedCharacterComics.stream().forEach(relItem -> {
      if (charItem.equals(relItem)) {
        relatedComics.add(relItem);
      }
    }));
    return relatedComics;
  }

  private MarvelCharacter retrieveCharacterFromMarvel(final String characterName){
    final MarvelCharacter character = makeCharacterRequest(characterName);
    return characterService.save(character);
  }

  private List<Comic> retrieveCharacterComicsFromMarvel(final String characterId){
    final List<Comic> comics = makeComicsRequest(characterId);
    return comicService.saveAll(comics);
  }

  private MarvelCharacter makeCharacterRequest(final String characterName) {
    final MarvelHashTO hash = authenticationService.generateHash();
    Client client = ClientBuilder.newClient( new ClientConfig().register( LoggingFilter.class ) );
    WebTarget webTarget = client.target(urlCharacter)
      .queryParam("name", characterName)
      .queryParam("ts", hash.getHashTimestamp())
      .queryParam("apikey", hash.getApiKey())
      .queryParam("hash", hash.getHashValue());
    Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
    Response response = invocationBuilder.get();

    if (response.getStatus() != 200) {
      throw new MarvelApiRequestError("Error make request to marvel api");
    }
    String json = response.readEntity(String.class);
    Gson gson = new Gson();
    Type type = new TypeToken<MarvelWrapperTO<DataCharacterWrapperTO>>(){}.getType();
    MarvelWrapperTO charactersWrapperTO = gson.fromJson(json, type);

    DataCharacterWrapperTO data = (DataCharacterWrapperTO) charactersWrapperTO.getData();
    Optional<MarvelCharacter> character = data.getResults().stream().filter(r ->
      r.getCharacterName() != null &&
        r.getCharacterName().trim().toLowerCase().equals(characterName.trim().toLowerCase())).findFirst();
    if (!character.isPresent()) {
      throw new MarvelApiRequestError(String.format("Character not found with name %s", characterName));
    }
    return character.get();
  }

  private List<Comic> makeComicsRequest(final String characterId) {
    final MarvelHashTO hash = authenticationService.generateHash();
    Client client = ClientBuilder.newClient( new ClientConfig().register( LoggingFilter.class ) );
    WebTarget webTarget = client.target(urlCharacter).path(characterId).path("comics")
      .queryParam("ts", hash.getHashTimestamp())
      .queryParam("apikey", hash.getApiKey())
      .queryParam("hash", hash.getHashValue());
    Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
    Response response = invocationBuilder.get();

    if (response.getStatus() != 200) {
      throw new MarvelApiRequestError("Error make request to marvel api");
    }
    String json = response.readEntity(String.class);
    Gson gson = new Gson();
    Type type = new TypeToken<MarvelWrapperTO<DataComicsWrapperTO>>(){}.getType();
    MarvelWrapperTO charactersWrapperTO = gson.fromJson(json, type);

    DataComicsWrapperTO data = (DataComicsWrapperTO) charactersWrapperTO.getData();
    return data.getResults();
  }
}
