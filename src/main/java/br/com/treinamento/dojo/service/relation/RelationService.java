package br.com.treinamento.dojo.service.relation;

import br.com.treinamento.dojo.exception.InconsistentRelationException;
import br.com.treinamento.dojo.exception.RelationNotFoundException;
import br.com.treinamento.dojo.model.Relation;
import br.com.treinamento.dojo.model.to.RelationTO;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by leandro on 19/11/16.
 */
public interface RelationService {
  List<RelationTO> findAll() throws InconsistentRelationException;

  Boolean delete(String relationId) throws RelationNotFoundException;

  RelationTO find(String relationId) throws RelationNotFoundException;

  RelationTO save(Relation relation);

  RelationTO edit(String relationId, Relation relation);

  Boolean hasIdsValidReference(String idRelation, Relation relation);

  Optional<Map.Entry<String, Relation>> findRelationByCharacterAndRelated(String characterId, String characterRelatedId);

  RelationTO instanceRelationTO(String id, Relation relation);
}
