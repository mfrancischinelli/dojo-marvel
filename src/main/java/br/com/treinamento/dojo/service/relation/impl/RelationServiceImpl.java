package br.com.treinamento.dojo.service.relation.impl;

import br.com.treinamento.dojo.exception.*;
import br.com.treinamento.dojo.model.Relation;
import br.com.treinamento.dojo.model.to.RelationTO;
import br.com.treinamento.dojo.service.relation.CharacterService;
import br.com.treinamento.dojo.service.relation.ComicService;
import br.com.treinamento.dojo.service.relation.RelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Created by leandro on 19/11/16.
 */
@Service
public class RelationServiceImpl implements RelationService {
  private Map<String, Relation> relationsBean;

  @Autowired
  private ApplicationContext context;

  @Autowired
  private CharacterService characterService;

  @Autowired
  private ComicService comicService;

  @PostConstruct
  private void init() {
    relationsBean =
      (Map<String, Relation>) context.getBean("relationsBean");
  }

  @Override
  public List<RelationTO> findAll() throws InconsistentRelationException {
    List<RelationTO> relations = new ArrayList<>();
    relationsBean.entrySet().stream().forEach(entry -> relations.add(instanceRelationTO(entry.getKey(), entry.getValue())));
    return relations;
  }

  @Override
  public Boolean delete(final String relationId) throws RelationNotFoundException {
    performCheckRelationExistence(relationId);
    relationsBean.remove(relationId);
    return Boolean.TRUE;
  }

  @Override
  public RelationTO find(final String relationId) throws RelationNotFoundException {
    performCheckRelationExistence(relationId);
    return instanceRelationTO(relationId, relationsBean.get(relationId));
  }

  @Override
  public RelationTO save(final Relation relation) {
    UUID relationId = UUID.randomUUID();
    performCheckIntegrity(relationId.toString(), relation,
      String.format("A Relation object has a inconsistent relationship. Relation id is %s.", relationId));
    Optional<Map.Entry<String,Relation>> relationEntry =
      findRelationByCharacterAndRelated(relation.getCharacterId(), relation.getRelatedCharacterId());
    if (relationEntry.isPresent()) {
      throw new InvalidRelationUpdateException("There is another relation with this characters.");
    }
    return includeOnBean(relationId.toString(), relation);
  }

  @Override
  public RelationTO edit(final String relationId, final Relation relation) {
    performCheckRelationExistence(relationId);
    performCheckIntegrity(relationId, relation,
      String.format("A Relation object has a inconsistent relationship. Relation id is %s.", relationId));
    Optional<Map.Entry<String,Relation>> relationEntry =
      findRelationByCharacterAndRelated(relation.getCharacterId(), relation.getRelatedCharacterId());
    if (relationEntry.isPresent() && !relationEntry.get().getKey().equals(relationId)) {
      throw new InvalidRelationUpdateException("There is anoter relation with this characters.");
    }
    return includeOnBean(relationId, relation);
  }

  @Override
  public Boolean hasIdsValidReference(final String relationId, final Relation relation) {
    Boolean isValid = Boolean.TRUE;
    try {
      characterService.findCharacterById(relation.getCharacterId());
      characterService.findCharacterById(relation.getRelatedCharacterId());
      comicService.findComicsByIds(relation.getComics());
    } catch (MarvelCharacterNotFoundException | ComicNotFoundException e) {
      isValid = Boolean.FALSE;
    }
    return isValid;
  }

  @Override
  public Optional<Map.Entry<String, Relation>> findRelationByCharacterAndRelated(final String characterId,
                                                                                 final String characterRelatedId) {
    return relationsBean.entrySet().stream().filter(relation ->
      (relation.getValue().getCharacterId().equals(characterId) ||
      relation.getValue().getRelatedCharacterId().equals(characterId)) &&
      (relation.getValue().getCharacterId().equals(characterRelatedId) ||
      relation.getValue().getRelatedCharacterId().equals(characterRelatedId))).findAny();
  }

  @Override
  public RelationTO instanceRelationTO(final String id, final Relation relation) {
    try {
      return RelationTO.builder()
        .relationId(id)
        .character(characterService.findCharacterById(relation.getCharacterId()))
        .relatedCharacter(characterService.findCharacterById(relation.getRelatedCharacterId()))
        .comics(comicService.findComicsByIds(relation.getComics())).build();
    } catch (MarvelCharacterNotFoundException | ComicNotFoundException e) {
      throw new
        InconsistentRelationException(
        String.format("A Relation object has a inconsistent relationship. Relation id is %s.", id));
    }
  }

  private void performCheckRelationExistence(String relationId) {
    if (!relationsBean.containsKey(relationId)) {
      throw new RelationNotFoundException(String.format("No relation found with id %s", relationId));
    }
  }

  private RelationTO includeOnBean(final String relationId, final Relation relation) {
    relationsBean.put(relationId, relation);
    return instanceRelationTO(relationId, relation);
  }

  private void performCheckIntegrity(String relationId, Relation relation, String exceptionMessage) {
    if (!hasIdsValidReference(relationId, relation)) {
      throw new
        InconsistentRelationException(
        exceptionMessage);
    }
  }
}
