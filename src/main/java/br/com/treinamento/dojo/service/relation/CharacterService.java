package br.com.treinamento.dojo.service.relation;

import br.com.treinamento.dojo.exception.MarvelCharacterNotFoundException;
import br.com.treinamento.dojo.model.MarvelCharacter;

import java.util.List;
import java.util.Optional;

/**
 * Created by leandro on 19/11/16.
 */
public interface CharacterService {
  MarvelCharacter findCharacterById(String id) throws MarvelCharacterNotFoundException;

  MarvelCharacter save(MarvelCharacter character);

  Optional<MarvelCharacter> findCharacterByName(String characterName);

  List<MarvelCharacter> findAll();

  MarvelCharacter find(String characterId);
}
