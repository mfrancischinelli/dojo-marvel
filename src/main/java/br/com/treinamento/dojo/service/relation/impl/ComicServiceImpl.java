package br.com.treinamento.dojo.service.relation.impl;

import br.com.treinamento.dojo.exception.ComicNotFoundException;
import br.com.treinamento.dojo.exception.MarvelCharacterNotFoundException;
import br.com.treinamento.dojo.model.Comic;
import br.com.treinamento.dojo.model.MarvelCharacter;
import br.com.treinamento.dojo.service.relation.ComicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by leandro on 19/11/16.
 */
@Service
public class ComicServiceImpl extends ComicService {
  @Autowired
  private ApplicationContext context;

  private Map<String, Comic> comicsBean;

  @PostConstruct
  private void init() {
    comicsBean = (Map<String, Comic>) context.getBean("comicsBean");
  }

  @Override
  public List<Comic> findComicsByIds(final List<String> ids) throws ComicNotFoundException {
    final List<Comic> comics = new ArrayList<>();
    for (String id : ids) {
      if (!comicsBean.containsKey(id)) {
        throw new ComicNotFoundException(String.format("Comic not found with id %s", id));
      }
      comics.add(comicsBean.get(id));
    }
    return comics;
  }

  @Override
  public List<Comic> saveAll(List<Comic> comics) {
    comics.stream().forEach(comic -> {
      if (!comicsBean.containsKey(comic.getComicId())) {
        comicsBean.put(comic.getComicId(), comic);
      }
    });
    return comics;
  }

  @Override
  public List<Comic> findAll() {
    return new ArrayList(comicsBean.values());
  }

  @Override
  public Comic find(String comicId) {
    return comicsBean.get(comicId);
  }
}
