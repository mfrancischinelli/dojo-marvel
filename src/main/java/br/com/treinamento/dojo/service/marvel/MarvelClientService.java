package br.com.treinamento.dojo.service.marvel;

import br.com.treinamento.dojo.model.to.CharactersTO;
import br.com.treinamento.dojo.model.to.RelationTO;

/**
 * Created by leandro on 09/11/16.
 */
public interface MarvelClientService {
  RelationTO save(CharactersTO characters);
}
