package br.com.treinamento.dojo.service.marvel.impl;

import br.com.treinamento.dojo.exception.UnexpectedErrorException;
import br.com.treinamento.dojo.model.to.MarvelHashTO;
import br.com.treinamento.dojo.service.marvel.MarvelAuthenticationService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * Created by leandro on 19/11/16.
 */
@Service
public class MarvelAuthenticationServiceImpl implements MarvelAuthenticationService {
  @Value("${marvel.api.public.key}")
  private String publicKey;

  @Value("${marvel.api.private.key}")
  private String privateKey;

  @Override
  public MarvelHashTO generateHash() {
    try {
      String ts = String.valueOf(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
      final String key = new StringBuilder().append(ts).append(privateKey).append(publicKey).toString();
      MessageDigest m = MessageDigest.getInstance("MD5");
      m.update(key.getBytes(),0,key.length());
      String hash = new BigInteger(1,m.digest()).toString(16);
      return MarvelHashTO.builder().hashTimestamp(ts).hashValue(hash).apiKey(publicKey).build();
    } catch (NoSuchAlgorithmException e) {
      throw new UnexpectedErrorException("Problem at generate marvel authentication key");
    }
  }
}
