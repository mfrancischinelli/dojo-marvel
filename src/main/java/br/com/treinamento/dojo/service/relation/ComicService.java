package br.com.treinamento.dojo.service.relation;

import br.com.treinamento.dojo.exception.ComicNotFoundException;
import br.com.treinamento.dojo.model.Comic;

import java.util.List;

/**
 * Created by leandro on 19/11/16.
 */
public abstract class ComicService {
  public abstract List<Comic> findComicsByIds(List<String> ids) throws ComicNotFoundException;

  public abstract List<Comic> saveAll(List<Comic> comics);

  public abstract List<Comic> findAll();

  public abstract Comic find(String comicId);
}
