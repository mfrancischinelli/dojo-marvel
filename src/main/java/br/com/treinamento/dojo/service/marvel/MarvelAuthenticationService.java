package br.com.treinamento.dojo.service.marvel;

import br.com.treinamento.dojo.model.to.MarvelHashTO;

/**
 * Created by leandro on 19/11/16.
 */
public interface MarvelAuthenticationService {
  MarvelHashTO generateHash();
}
