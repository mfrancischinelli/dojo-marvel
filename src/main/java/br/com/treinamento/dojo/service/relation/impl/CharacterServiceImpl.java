package br.com.treinamento.dojo.service.relation.impl;

import br.com.treinamento.dojo.exception.MarvelCharacterNotFoundException;
import br.com.treinamento.dojo.model.MarvelCharacter;
import br.com.treinamento.dojo.model.to.RelationTO;
import br.com.treinamento.dojo.service.relation.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by leandro on 19/11/16.
 */
@Service
public class CharacterServiceImpl implements CharacterService {

  @Autowired
  private ApplicationContext context;

  private Map<String, MarvelCharacter> characterBean;

  @PostConstruct
  private void init() {
    characterBean =
        (Map<String, MarvelCharacter>) context.getBean("charactersBean");
  }

  @Override
  public MarvelCharacter findCharacterById(final String id) throws MarvelCharacterNotFoundException {
    if (!characterBean.containsKey(id)) {
      throw new MarvelCharacterNotFoundException(String.format("Character not found with id %s", id));
    }
    return characterBean.get(id);
  }

  @Override
  public MarvelCharacter save(MarvelCharacter character) {
    if (!characterBean.containsKey(character.getCharacterId())) {
      characterBean.put(character.getCharacterId(), character);
    }
    return character;
  }

  @Override
  public Optional<MarvelCharacter> findCharacterByName(String characterName) {
    return characterBean.values().stream().filter(character ->
      character.getCharacterName().toLowerCase().trim().equals(characterName.toLowerCase().trim())).findFirst();
  }

  @Override
  public List<MarvelCharacter> findAll() {
    return new ArrayList(characterBean.values());
  }

  @Override
  public MarvelCharacter find(String characterId) {
    return characterBean.get(characterId);
  }
}
