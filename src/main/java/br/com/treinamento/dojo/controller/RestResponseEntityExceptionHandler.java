package br.com.treinamento.dojo.controller;

import br.com.treinamento.dojo.exception.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Created by leandro on 19/11/16.
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
  @ExceptionHandler(value = {InconsistentRelationException.class})
  @ResponseBody
  protected ResponseEntity<Object> handleInconsistentMarvelData(RuntimeException ex, WebRequest request) {
    return handleExceptionInternal(ex, ex.getMessage(),
      new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
  }

  @ExceptionHandler(value = {RelationNotFoundException.class})
  @ResponseBody
  protected ResponseEntity<Object> handleRelationNotFound(RuntimeException ex, WebRequest request) {
    return handleExceptionInternal(ex, ex.getMessage(),
      new HttpHeaders(), HttpStatus.NOT_FOUND, request);
  }

  @ExceptionHandler(value = {ComicNotFoundException.class})
  @ResponseBody
  protected ResponseEntity<Object> handleComicNotFound(RuntimeException ex, WebRequest request) {
    return handleExceptionInternal(ex, ex.getMessage(),
      new HttpHeaders(), HttpStatus.NOT_FOUND, request);
  }

  @ExceptionHandler(value = {MarvelCharacterNotFoundException.class})
  @ResponseBody
  protected ResponseEntity<Object> handleMarvelCharacterNotFound(RuntimeException ex, WebRequest request) {
    return handleExceptionInternal(ex, ex.getMessage(),
      new HttpHeaders(), HttpStatus.NOT_FOUND, request);
  }

  @ExceptionHandler(value = {InvalidParameterException.class})
  @ResponseBody
  protected ResponseEntity<Object> handleInvalidParameterException(RuntimeException ex, WebRequest request) {
    InvalidParameterException ire = (InvalidParameterException) ex;
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    return handleExceptionInternal(ex, ire.getMessage(),
      new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
  }

  @ExceptionHandler(value = {MarvelApiRequestError.class})
  @ResponseBody
  protected ResponseEntity<Object> handleMarvelCharacterNotFoundException(RuntimeException ex, WebRequest request) {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    return handleExceptionInternal(ex, ex.getMessage(),
      new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
  }
}