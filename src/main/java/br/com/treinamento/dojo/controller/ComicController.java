package br.com.treinamento.dojo.controller;

import br.com.treinamento.dojo.model.Comic;
import br.com.treinamento.dojo.model.MarvelCharacter;
import br.com.treinamento.dojo.service.relation.ComicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/public")
public class ComicController {
  @Autowired
  private ComicService comicService;

  @RequestMapping(value = "/comics", method = RequestMethod.GET)
  @ResponseBody
  @Produces("application/json")
  public ResponseEntity<List<Comic>> findAll() {
    return new ResponseEntity<>(comicService.findAll(), HttpStatus.FOUND);
  }

  @RequestMapping(value = "/comics/{comicId}", method = RequestMethod.GET)
  @ResponseBody
  @Produces("application/json")
  public ResponseEntity<Comic> find(@PathVariable(value="comicId") String comicId) {
    return new ResponseEntity<>(comicService.find(comicId), HttpStatus.FOUND);
  }
}
