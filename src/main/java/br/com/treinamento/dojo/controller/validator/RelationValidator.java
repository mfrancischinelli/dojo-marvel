package br.com.treinamento.dojo.controller.validator;

import br.com.treinamento.dojo.model.Relation;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by leandro on 19/11/16.
 */
@Component
public class RelationValidator implements Validator {
  @Override
  public boolean supports(Class clazz) {
    return Relation.class.equals(clazz);
  }

  @Override
  public void validate(Object o, Errors errors) {
    Relation relation = (Relation) o;

    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "characterId", "error.characterId", "characterId is required.");
    ValidationUtils
      .rejectIfEmptyOrWhitespace(errors, "relatedCharacterId", "error.relatedCharacterId", "relatedCharacterId is required.");
    ValidationUtils.rejectIfEmpty(errors, "comics", "error.comics", "comics must have be at last one value");

    if (isCharactersEquals(relation)) {
      errors.reject("characterId can not be equals to relatedCharacterId");
    }
  }

  private boolean isCharactersEquals(Relation relation) {
    return relation != null && relation.getCharacterId() != null && relation.getRelatedCharacterId() != null
      && relation.getCharacterId().equals(relation.getRelatedCharacterId());
  }
}
