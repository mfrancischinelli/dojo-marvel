package br.com.treinamento.dojo.controller;

import br.com.treinamento.dojo.controller.validator.MarvelImportValidator;
import br.com.treinamento.dojo.exception.InvalidParameterException;
import br.com.treinamento.dojo.model.to.CharactersTO;
import br.com.treinamento.dojo.model.to.RelationTO;
import br.com.treinamento.dojo.service.marvel.MarvelClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;

@RestController
@RequestMapping(value = "/v1/public")
public class MarvelImportController {
	@Autowired
	private MarvelClientService marvelClientService;

  @Autowired
  private MarvelImportValidator validator;

  @RequestMapping(value = "/characters/comics/relations", method = RequestMethod.POST, headers="Accept=application/json",  consumes = "application/json")
  @ResponseBody
  @Produces("application/json")
  public ResponseEntity<RelationTO> save(@RequestBody CharactersTO characters, BindingResult result) {
    validator.validate(characters, result);
    if (result.hasErrors()) {
      throw new InvalidParameterException("Invalid parameters found.", result);
    }
    final RelationTO relation = marvelClientService.save(characters);
    return new ResponseEntity<>(relation, (relation == null)? HttpStatus.NO_CONTENT:HttpStatus.CREATED);
  }
}
