package br.com.treinamento.dojo.controller.validator;

import br.com.treinamento.dojo.model.to.CharactersTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by leandro on 19/11/16.
 */
@Component
public class MarvelImportValidator implements Validator {
  @Override
  public boolean supports(Class clazz) {
    return CharactersTO.class.equals(clazz);
  }

  @Override
  public void validate(Object o, Errors errors) {
    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "characterName", "error.characterName", "characterName is required.");
    ValidationUtils
      .rejectIfEmptyOrWhitespace(errors, "relatedCharacterName", "error.relatedCharacterName", "relatedCharacterName is required.");
  }
}
