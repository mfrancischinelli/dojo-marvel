package br.com.treinamento.dojo.controller;

import br.com.treinamento.dojo.model.MarvelCharacter;
import br.com.treinamento.dojo.service.relation.CharacterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/public")
public class CharacterController {
	@Autowired
	private CharacterService characterService;

  @RequestMapping(value = "/characters", method = RequestMethod.GET)
  @ResponseBody
  @Produces("application/json")
  public ResponseEntity<List<MarvelCharacter>> findAll() {
    return new ResponseEntity<>(characterService.findAll(), HttpStatus.FOUND);
  }

  @RequestMapping(value = "/characters/{characterId}", method = RequestMethod.GET)
  @ResponseBody
  @Produces("application/json")
  public ResponseEntity<MarvelCharacter> find(@PathVariable(value="characterId") String characterId) {
    return new ResponseEntity<>(characterService.find(characterId), HttpStatus.FOUND);
  }
}
