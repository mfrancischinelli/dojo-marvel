package br.com.treinamento.dojo.controller;

import br.com.treinamento.dojo.controller.validator.RelationValidator;
import br.com.treinamento.dojo.exception.InvalidParameterException;
import br.com.treinamento.dojo.model.Relation;
import br.com.treinamento.dojo.model.to.RelationTO;
import br.com.treinamento.dojo.service.relation.RelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Produces;
import java.util.List;

@RestController
@RequestMapping(value = "/v1/public")
public class RelationController {
	@Autowired
	private RelationService relationService;

  @Autowired
  private RelationValidator validator;

  @RequestMapping(value = "/relations", method = RequestMethod.POST, headers="Accept=application/json",  consumes = "application/json")
  @ResponseBody
  @Produces("application/json")
  public ResponseEntity<RelationTO> save(@RequestBody Relation relation, BindingResult result) {
    validator.validate(relation, result);
    if (result.hasErrors()) {
      throw new InvalidParameterException("Invalid parameters found.", result);
    }
    return new ResponseEntity<>(relationService.save(relation), HttpStatus.CREATED);
  }

  @RequestMapping(value = "/relations/{relationId}", method = RequestMethod.PUT, headers="Accept=application/json",  consumes = "application/json")
  @ResponseBody
  @Produces("application/json")
  public ResponseEntity<RelationTO> edit(@PathVariable(value="relationId") String relationId,
                                         @RequestBody Relation relation, BindingResult result) {
    validator.validate(relation, result);
    if (result.hasErrors()) {
      throw new InvalidParameterException("Invalid parameters found.", result);
    }
    return new ResponseEntity<>(relationService.edit(relationId, relation), HttpStatus.OK);
  }

  @RequestMapping(value = "/relations", method = RequestMethod.GET)
  @ResponseBody
  @Produces("application/json")
  public ResponseEntity<List<RelationTO>> findAll() {
    return new ResponseEntity<>(relationService.findAll(), HttpStatus.FOUND);
  }

  @RequestMapping(value = "/relations/{relationId}", method = RequestMethod.DELETE)
  @ResponseBody
  @Produces("application/json")
  public ResponseEntity delete(@PathVariable(value="relationId") String relationId) {
    relationService.delete(relationId);
    return new ResponseEntity(HttpStatus.NO_CONTENT);
  }

  @RequestMapping(value = "/relations/{relationId}", method = RequestMethod.GET)
  @ResponseBody
  @Produces("application/json")
  public ResponseEntity<RelationTO> find(@PathVariable(value="relationId") String relationId) {
    return new ResponseEntity<>(relationService.find(relationId), HttpStatus.FOUND);
  }
}
